
## Coarse Tater Five

An anagram solver, hardcoded for [cordial minuet](http://cordialminuet.com/)'s puzzle. Uses opsd.txt (scrabble word list) from [https://github.com/dolph/dictionary] (there called ospd.txt).

&copy; Nicholas Prado but released under fair license terms, as described at http://fairlicense.org
