
'''
get all applicable words
	read word-file line wise
	if it is x chars long and has letter y in position z, save
	persist this as a file
find pairs
	classify by letter groups
	pair any from complementary word groups
show
'''

def presentResults( dictOfAnagrams ) :
	for leftPiece in dictOfAnagrams :
		for rightPiece in dictOfAnagrams[ leftPiece ] :
			print( leftPiece +" "+ rightPiece +"\r\n" )


def getAnagramSet() :
	return ['c', 'o', 'r', 'd', 'i', 'a', 'l', 'm', 'i', 'n', 'u', 'e', 't']


def onlyHasValidFor( phrase, lettersLeft ) :
	for particle in phrase :
		if len( lettersLeft ) == 0 :
			return False
		elif particle not in lettersLeft :
			return False
		else :
			lettersLeft.remove( particle )
	return True


def onlyHasValid( phrase ) :
	return onlyHasValidFor( phrase, getAnagramSet() )


def lettersExceptFrom( word, lettersLeft ) :
	for particle in word :
		if len( lettersLeft ) == 0 :
			return lettersLeft
		else :
			lettersLeft.remove( particle )
	return lettersLeft


def matchesFor( leftWord, potentialPairings ) :
	validPairings = []
	canonLettersForSecond = lettersExceptFrom( leftWord, getAnagramSet() )
	for candidate in potentialPairings :
		letters = canonLettersForSecond[:]
		# NOTE slice is a medium copy (ie not recursive)
		if onlyHasValidFor( candidate, letters ) :
			validPairings.append( candidate )
	return validPairings


def dictOfPairings( firstAndSecondAsList ) :	
	pairings = {}
	for leftWord in firstAndSecondAsList[0] :
		matches = matchesFor( leftWord, firstAndSecondAsList[1] )
		if len( matches ) > 0 :
			pairings[ leftWord ] = matches
	return pairings


def matchesSecondStyle( phrase ) :
	return len( phrase ) == 6 \
		and phrase[ 1 ] == 'i' \
		and onlyHasValid(phrase) \
		and 'e' not in phrase


def matchesFirstStyle( phrase ) :
	return len( phrase ) == 7 \
		and phrase[ 1 ] == 'e' \
		and onlyHasValid(phrase) \
		and phrase.count( 'i' ) < 2


def getInitialList() :
	firstStyleWords = []
	secondStyleWords = []
	with open( "scrabblewordlist.txt" ) as file :
		for line in file :
			line = line[:-1]
			if matchesFirstStyle( line ) :
				firstStyleWords.append( line )
			elif matchesSecondStyle( line ) :
				secondStyleWords.append( line )
	words = []
	if len( firstStyleWords ) > 0 :
		words.append( firstStyleWords )
	if len( secondStyleWords ) > 0 :
		words.append( secondStyleWords )
	return words


def solveCordialMinuetAnagram() :
	allPotential = getInitialList()
	if len( allPotential ) < 1 :
		print( "there aren't any of some type, the opposite of "+ allPotential[0][0] )
		return
		# I know there's at least one word from hand tests and grade school
	validSolutions = dictOfPairings( allPotential )
	presentResults( validSolutions )

'''
- dictionary oriented
X get all applicable words
	read word-file line wise
	if it is x chars long and has letter y in position z and only has the anagram letters
		save
	(persist this as a file?)
find pairs
	classify by letter groups
	pair any from complementary word groups
show

-anagram oriented
combine the available letters
filter to the valid dictionary words
'''

solveCordialMinuetAnagram()



















